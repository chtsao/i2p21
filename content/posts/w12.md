---
title: "Week 12. Quantile and Normal Distribution"
date: 2021-12-16T08:35:55+08:00
draft: false
tags: [quantile, normal, standard normal]
---
![](https://miro.medium.com/max/573/1*3x5SIG4T9dD0G_PfgT6iig.jpeg)
Image by Gerd Altmann from Pixbay

## Identification of a r.v.

我們如何認出一個隨機變數---換個角度來說，一個隨機變數的 身份證 ID 是什麼？ 已討論過的 ID 有

* pmf for a discrete random variable (or pdf for a continuous random variable)
* distribution function, df,  (also known as cumulative distribution function, cdf )

另外一個重要的 ID 就是 moment generating function (mgf)

Let $X$ be a random variable then its mgf is defined as 

$M(t) =  E(e^{tX})$ if exists. 

Moment generating function 有兩個重要用途

1. 小用：計算動差. For $r \in \mathcal{N}$, the $r$-th moment of $X$ is  $E(X^r)$ if exists. It can be computed through mgf. 

    $$ E(X^r) = M^{(r)}(0)$$ if exists. 

2. 大用：認出 Identify a random variable.

### Homework 4
Textbook Sec 5.8 (Page 68): 5.11–5.14; Sec 7.6 (Page 100-101):7.7--7.12.  Sec 8.6 (Page 112): 8.9, 8.10 (Write these probabilities as functions of $\Phi$'s then look up Table B.1 or compute numerically, see [How](https://chtsao.gitlab.io/i2p2020/posts/w11/#how)). Submit online @ by 12/23. 

### Study less, study smart
* Study Less Study Smart”by Dr. Marty Lobdell: [summary by UAPB](https://www.uapb.edu/sites/www/Uploads/SSC/Study%20Smarter%20Not%20Harder.pdf), [video](https://www.youtube.com/watch?v=IlU-zDU6aQ0)
* [學得更好-更開心-更有效率](https://chtsao.gitlab.io/i2p2019/#%E4%BD%A0%E5%8F%AF%E4%BB%A5%E5%AD%B8%E5%BE%97%E6%9B%B4%E5%A5%BD-%E6%9B%B4%E9%96%8B%E5%BF%83-%E6%9B%B4%E6%9C%89%E6%95%88%E7%8E%87)
*  Mike and Matty: [The REAL Reason Why You Get Bad Grades](https://youtu.be/GJ_o-1bfz-M), [Evidence based learning strategies](https://youtu.be/UEJmgaFQUH8)

### $X, aX+b$ and $Z=\frac{X-\mu}{\sigma}$

常態分配可說是統計分析、資料科學中相當重要而常用的一個分配。以下的這一個結果, 可以幫忙我們推導出很多重要的性質，推導本身也相當具代表性。提示：定義出發，相信符號，預見結果，尋找胚騰 (pattern)＋耐心與好奇。

* If $X \sim N(\mu, \sigma^2)$ then for any $a \neq 0, b \in \mathcal{R},$ 

  $$Y=aX +b \sim N(a \mu+b, a^2 \sigma^2)$$ 

* Particularly, let $a=\frac{1}{\sigma}$ and $b=\frac{-\mu}{\sigma},$ 

   $$Z=\frac{X-\mu}{\sigma} \sim N(0, 1).$$

* This transformation is often referred as **standardization**. It transforms all normal to standard normal, N(0,1).  Therefore any probabilities such as  $P(X \leq a)  $ or $P(a \leq X \leq b) $ of a normal random variable can be computed through standard normal $Z \sim N(0, 1).$

* $X \sim N(\mu, \sigma^2)$. It can be shown that $E(X) = \mu$ and $Var(X) = \sigma^2.$  這個結果有兩個重要意義

  * 常態分配中參數 $\mu, \sigma^2$ 是有意義的參數-- $\mu = E(X), \sigma^2 = Var(X)$ 分別是這個分配中心與分散程度的一種刻劃。
  * 常態分配只需要刻劃中心 $E(X)$ 與 $Var(X)$ 就可機率/統計決定這個分配，進而計算相關機率或統計性質。

The properties above can be established by either 
* derive cdf (or pdf) of $Y$, or
* derive the mgf  of $Y$
and recognize its distribution and corresponding parameters. 

### Compute $E(X)$ and $Var(X)$ 
Recall that if $X$ is a continuous random variable with pdf $f$ then  
$$ E(X) = \int_{-\infty}^\infty x f(x) dx \ \text{if  exists}$$
$$ Var(X) = E[(X - E(X))^2] \ \text{if  exists}$$
$$(E(g(X))= \int_{-\infty}^\infty g(x) f(x) dx \ \text{if  exists}).$$

## Quantile

以考試成績為例，你可能會想知道在班上大約排在什麼位置。去除個資匿名地給出整班的分數對應排名有許多方法，如枝葉圖(stem-and-leaf)。如果只給幾個數字, five-point-summary ＝ (min, Q1, Q2=median, Q3, max) 也是一個好方式。其中的 Q1, Q2, Q3 就是 1st, 2nd, 3rd quartiles, 一些特殊的 *quantiles*. 

**Definition** (Page 66). Let $X$ be a continuous r.v. with pdf $f$ and $0 \leq p \leq 1$. The *pth quantile* or 1*00p-th percentile* of $X$ is the smallest number $q_p$ such that 

$$F(q_p) = P(X \leq q_p) = p.$$ 

The median of $X$ is its $50$th quantile and the second quartile or $Q_2$. and $Q_1$, the first quartile =the 25th quantile.