---
title: "Week 10. Midterm Reflection"
date: 2021-12-02T06:20:43+08:00
draft: false
tags: [midterm, remark]
---

<img src="https://i0.wp.com/cdn.business2community.com/wp-content/uploads/2013/11/Screen-shot-2013-11-11-at-11.43.55-AM.png" style="zoom:67%;" />

### 期中考成績

Five-point summary:
```
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
   5.00   34.75   60.00   54.50   72.25  100.00 
```
s.d.=24.8; n=64; 年級 2 = 42; 年級 2+ = 22.  

Stem-and-leaf
```
  The decimal point is 1 digit(s) to the right of the |

   0 | 58
   1 | 066
   2 | 014558
   3 | 001245588
   4 | 011558
   5 | 00456
   6 | 00000000133489
   7 | 022357
   8 | 05557889
   9 | 0035
  10 | 0
```

[進一步圖表分析](https://chtsao.gitlab.io/i2p21/midcal.nb.html), [i2p2020mid](http://faculty.ndhu.edu.tw/~chtsao/ftp/i2p/i2pmid2020rv.nb.html), 鑑往*知*來([2016stat](http://faculty.ndhu.edu.tw/~chtsao/ftp/rg.w08.nb.html), [2019I2p](http://faculty.ndhu.edu.tw/~chtsao/ftp/i2p/i2p2019w.nb.html), [2020stat](http://faculty.ndhu.edu.tw/~chtsao/ftp/i2p/stat20w.nb.html)) 

### Remark
1. 考得好的同學：保持這次的優勢，繼續努力，嘗試對這主題有更深的了解。讓這門課成為你知道真正知道的起點。
2. 考得不理想的同學：現在放棄，你等於提早出局。我教的課，不論期中多低分，都有過的機會。不要低估自己的可能，但更不要低估自己的惰性。有時偷點懶是人的天性，但不能將惰性放縱慣養，讓自己成為它的奴隸。這門課就是一個與它的對戰。成功與失敗都不是一個結果，而是習慣。課程及格不及格當然有影響，但慢慢養成（促成）成功的習慣，遠離失敗的習慣，更為重要。
3. [關於棄選](http://faculty.ndhu.edu.tw/~chtsao/ftp/i2p/withdrawl.html)

### 學習學習/專業或大學科目學習

##  Study less, study smart
* Study Less Study Smart”by Dr. Marty Lobdell: [summary by UAPB](https://www.uapb.edu/sites/www/Uploads/SSC/Study%20Smarter%20Not%20Harder.pdf), [video](https://www.youtube.com/watch?v=IlU-zDU6aQ0)
* [學得更好-更開心-更有效率](https://chtsao.gitlab.io/i2p2019/#%E4%BD%A0%E5%8F%AF%E4%BB%A5%E5%AD%B8%E5%BE%97%E6%9B%B4%E5%A5%BD-%E6%9B%B4%E9%96%8B%E5%BF%83-%E6%9B%B4%E6%9C%89%E6%95%88%E7%8E%87)
*  Mike and Matty: [The REAL Reason Why You Get Bad Grades](https://youtu.be/GJ_o-1bfz-M), [Evidence based learning strategies](https://youtu.be/UEJmgaFQUH8)
