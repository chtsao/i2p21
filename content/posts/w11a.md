---
title: "Week 11.1. Normal Random Variable"
date: 2021-12-07T06:12:23+08:00
draft: false
tags: [normal r.v., quantile, standard normal]
---

<img src="https://images.squarespace-cdn.com/content/v1/5d10ccef396a6b000140f6e0/1594896649088-A97DPDKU0D2BAZTVOR1H/ke17ZwdGBToddI8pDm48kDHPSfPanjkWqhH6pl6g5ph7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0mwONMR1ELp49Lyc52iWr5dNb1QJw9casjKdtTg1_-y4jz4ptJBmI9gQmbjSQnNGng/29.jpg?format=1000w" style="zoom:67%;" />
 [Sam Lock Art](https://www.samlock.com/)
### Normal = 常態？
一般常將 normal distribution 翻譯為常態分配。這沒有錯，但可能限縮了normal 的意義。

* 規範或模範：如[National Normal Univeristy](http://en.ntnu.edu.tw/) 並不宜理解為台灣正常大學，其中 normal 有 學為人師，行為世範 中 **範**的意義。
* 常態與其說是正常狀態，不如理解經常出現的狀態，這當然也是相對的說法。如在一個群體中所謂正常或常出現的某些個性特質的人，在其他群體並不見得如此。

### Why normal?
* 立即可用：有許多的分配由過去使用經驗可以知道，normal 都是一個相當不錯的分配假設，如某（性質類似）群體的身高、體重、IQ、分數等。
* 逼近其他常用分配：常態分配可以用來逼近相當多其他的分配。最重要也廣泛使用的，應該是中央極限定理所說，在適當條件下大樣本情形，常態分配可以逼近和或平均 ( $\sum_{i=1}^n X_i$, $\frac{1}{n}\sum_{i=1}^n X_i$ ).
* 作為誤差的一個典範模型：測量誤差是許多統計應用終必需要控制的一環，若誤差服從常態分配+ iid (independently identically distributed) 假設，可以讓許多分析變得簡單而有效率。
* 方便：數學、統計或在計算上，常態分配可以說是最方便的模型之一。

### What 
We say $X$ is a normal random variable with mean $\mu \in \mathcal{R}$ and variance $\sigma^2$, denoted as $X \sim N(\mu, \sigma^2)$ with pdf 

$$f(x) = \frac{1}{\sqrt{2\pi}\sigma^2} e^{-\frac{(x-\mu)^2}{2 \sigma^2} } $$

for all $x \in \mathcal{R}$ and cdf

$$ F(x) = \int_{-\infty}^x f(t) dt. $$

### How
 $X \sim N(\mu, \sigma^2)$. Compute 
	a. $P(X \leq a)$
	b. $P(X > a)$
	c. $P(a <X < b)$
	d. Find $q_p$ such that $P(X \leq q_p) = p, p \in  (0,1) $

* By Standard normal table: rewrite these prob in terms of functions of  
$\Phi$'s, cdf of standard normal then look up the table. for example [z-table](http://www.z-table.com/)
* [Normal distribution computing](http://onlinestatbook.com/2/calculators/normal_dist.html) @[onlinestatbook.com](http://onlinestatbook.com/)	