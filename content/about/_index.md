---
title: "About"
date: 2021-09-9T07:03:02+08:00
draft: false
categores: [adm]
---
![Cheat](https://upload.wikimedia.org/wikipedia/commons/4/46/Falschspieler.jpg)


* [Syllabus]()    Curator: C. Andy Tsao  Office: SE A411.  Tel: 3520
* Lectures: Tue. 1310-1500, Thr. 1610-1700 
  * 3D: @ A316; 
  * Virtual: [Google Class](https://classroom.google.com/c/MzkxOTcwNDMyMDQ1?cjc=eyqq5ez), [Google Meet](https://meet.google.com/hpb-oojd-hte)
* Office Hours:  MON 14:10-15:00；THU 15:10-16:00 @ SE A411 or by appointment.

* TA Office Hours: 
	* 蘇羿豪 <611011102@gms.ndhu.edu.tw><038903517>  
		* 星期二 1600-1700，A408. Or Google_meet：https://meet.google.com/tod-apky-kcb
	* 呂一昕 <610911007@gms.ndhu.edu.tw><038903537> 
		* 星期五 1300-1400，A412.  Or Google_meet: :https://meet.google.com/phz-thda-sxc

* Prerequisites: Calculus

* Textbook:  Dekking, Kraaikamp, Lopuhaä and Meester (2005). A Modern Introduction to Probability and Statistics: Understanding Why and How. Springer, London. [Legal downloadable from NDHU](http://134.208.29.176:8080/toread/opac/bibliographic_view?NewBookMode=false&id=766570&q=Modern+Introduction+to+Probability+and+STatistics&start=0&view=CONTENT)

